package com.nijjwal.day4.anonymous.projec2;

public class Test2 {

	public static void main(String... args) {

		Runnable r = () -> {
			System.out.println(
					"Create an anonymous inner class by implementing Runnable interfaec, but dont' use the keyword implements");
		};

		Thread t = new Thread(r);
		t.start();

	}

}
