package com.nijjwal.day4.anonymous.projec1;

public class Test1 {

	public static void main(String... args) {

		Thread t = new Thread() {

			@Override
			public void run() {
				System.out.println(
						"Create an anonymous inner class by extending Thread class, but don't use the keyword extends.");
			}

		};

		t.start();

	}

}
